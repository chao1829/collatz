#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C) 2016
# Glenn P. Downing
# ---------------------------


# ------------
# collatz_read
# ------------

def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing two ends of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------


def collatz_eval(i, j):
    """
    i, j the beginning or end of the range, inclusive
    Splits the given range into cached and non-cached ranges
    Searches cached ranges and stores max cycle length 
    Searches non-cached ranges and compares max cycle length to cached result
    return the max cycle length of the range [i, j]
    """
    # Test to ensure input is valid and within range
    assert type(i) == int and type(j) == int, "Please enter integers only"
    assert 0 < i < 1000000, "Numbers out of range 1-999,999"
    assert 0 < j < 1000000, "Numbers out of range 1-999,999"

    # Create range in ascending order

    if i < j:
        r = range(i, j + 1)
    else:
        r = range(j, i + 1)

    # Create ranges for numbers to test outside of cached ranges

    toSplit = list(r)
    preCache = []
    postCache = []
    for n in toSplit:
        if n % 1000 == 0 and (n + 999) in r:
            preCache = toSplit[:toSplit.index(n)]
        elif len(r) > 1001 and n % 1000 == 0 and (n + 999) not in r:
            postCache = toSplit[toSplit.index(n):]

    # Create a dictionary with ranges of 1000 and associated collatz values from a txt file for a cache
    cache = {}
    with open("eagerCache.txt") as bank:
        for line in bank:
            (key1, key2, val) = line.split()
            key = range(int(key1), int(key2))
            val = int(val)
            cache[key] = val

    maxCycle = 0

    # determine max cycle length from cached values within the given range
    for item in cache:
        if item.start in r and item[-1] in r:
            if cache[item] > maxCycle:
                maxCycle = cache[item]

    # determine max collatz cycle length by testing ranges outside of cached ranges
    if preCache == postCache == []:
        finalIter = r
    else:
        finalIter = preCache + postCache

    for num in finalIter:
        cycleCount = 1
        while num > 1:
            if num % 2 == 0:
                num /= 2
            else:
                num = 3 * num + 1
            cycleCount += 1
        if cycleCount > maxCycle:
            maxCycle = cycleCount

    # Test to ensure output is valid and expected value
    assert type(maxCycle) == int, "Something went wrong..."
    assert maxCycle > 0, "Something went wrong :("
    return maxCycle

# -------------
# collatz_print
# -------------


def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    Runs the functions necessary to solve and output 
    the solution to the Collatz function
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
